Name:           gnome-shell-extension-music-control
Version:        1.0
Release:        1%{?dist}
Summary:        Gnome shell extension to control music playback
Group:          User Interface/Desktops
License:        GPLv2+
# Doesn't have its own URL yet
#URL:            http://
Source0:        http://venemo.fedorapeople.org/sources/%{name}-%{version}.tar.gz
BuildArch:      noarch
Requires:       gnome-shell >= 3.0.0.2

%description 
%{name} is a simple Gnome shell extension
that can control music playback.

%prep
%setup -q

%build
%{nil}

%install
rm -rf $RPM_BUILD_ROOT
install -d $RPM_BUILD_ROOT%{_datadir}/gnome-shell/extensions/music-control@venemo.net/
install -pm 644 extension.js $RPM_BUILD_ROOT%{_datadir}/gnome-shell/extensions/music-control@venemo.net/
install -pm 644 metadata.json $RPM_BUILD_ROOT%{_datadir}/gnome-shell/extensions/music-control@venemo.net/

%files
%defattr(-,root,root,-)
%{_datadir}/gnome-shell/extensions/music-control@venemo.net/extension.js
%{_datadir}/gnome-shell/extensions/music-control@venemo.net/metadata.json
%doc LICENSE

%changelog
* Sun May 8 2011 Timur Kristóf <venemo@fedoraproject.org> 1.0.0-1
- Initial version
